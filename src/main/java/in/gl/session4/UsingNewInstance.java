package in.gl.session4;

public class UsingNewInstance {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        Class cls = Class.forName("in.gl.session4.Employee");

        Employee employee = (Employee) cls.newInstance();
    }
}
