package in.gl.session6;

public class MyClass extends MyClone{

    private static MyClass instance;

    private MyClass(){

    }

//    @Override
//    protected Object clone()
//            throws CloneNotSupportedException
//    {
//        throw new CloneNotSupportedException("Sorry");
//    }
    public static synchronized MyClass getInstance(){
        if(instance == null){
            return instance = new MyClass();
        }else {
            return instance;
        }
    }
}
