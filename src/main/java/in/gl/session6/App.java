package in.gl.session6;

public class App {
    public static void main(String[] args) throws CloneNotSupportedException {

        MyClass instance1= MyClass.getInstance();
        System.out.println(instance1.hashCode());

        MyClass instance2= (MyClass) instance1.clone();
        System.out.println(instance2.hashCode());
    }
}
