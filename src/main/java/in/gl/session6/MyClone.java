package in.gl.session6;

public class MyClone implements Cloneable{
    @Override
    protected Object clone()
            throws CloneNotSupportedException
    {
        // Super() keyword refers to parent class
        return super.clone();
    }
}
