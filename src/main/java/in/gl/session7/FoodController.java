package in.gl.session7;

public class FoodController {

    @Wire
    private FoodService foodService ;

    public void handleCooking(){
        foodService.cookMeal();
    }
}
