package in.gl.session7;

import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.FieldPosition;
import java.util.Set;

public class MainWithReflection {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Reflections reflections = new Reflections(
                new ConfigurationBuilder()
                        .forPackage("in.gl.session7")
                        .filterInputsBy(new FilterBuilder().includePackage("in.gl.session7"))
                        .setScanners(new FieldAnnotationsScanner()));

// or similarly using the convenient constructor
//        Reflections reflections = new Reflections("com.my.project");


        Set<Field> fields = reflections.getFieldsAnnotatedWith(Wire.class);

        FoodController foodController = new FoodController();

        for(Field field: fields){
            System.out.println(field.getType());

            Class typeClass = field.getType();

            Constructor constructor = typeClass.getConstructor();

            Object oobj = constructor.newInstance();

            System.out.println(oobj);
            field.setAccessible(true);
            field.set(foodController, oobj);
        }

        foodController.handleCooking();



    }
}
