package in.gl.session1;

import com.sun.source.tree.ParameterizedTypeTree;

import java.lang.reflect.*;
import java.util.List;

// Even though we delete some of the fields it will run without error

public class Main {

    public static void main(String[] args) throws InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {



        Student student = new Student("Jai", "GL");

        Class studentClass = student.getClass();



        System.out.println("Class Name:: "+ studentClass.getName());

        System.out.println("Interface:: "+ studentClass.isInterface());

        System.out.println("===== Constructors =========");

        Constructor[] constructors = studentClass.getConstructors();

//        Student student1 = (Student) constructors[1].newInstance(String.class, String.class);

        for(Constructor constructor: constructors){
            System.out.println(constructor.getName());
            // check parametercount
            // print the parameter
            if(constructor.getParameters().length > 0){
                for(Parameter parameter: constructor.getParameters()){
                    System.out.println(parameter.getName() + " " +  parameter.getType());
                }
            }
        }

//        // Methods
//        Method[] methods = studentClass.getMethods();
//        for(int i=0; i< methods.length; i++){
//            System.out.println("Method "+(i+1) + Modifier.toString(methods[i].getModifiers())
//                    +" "+ methods[i].getReturnType().getName()+" "+ methods[i].getName());
//        }
//
        System.out.println("---------------");
        // getdeclared fields
//        Field[] fields = studentClass.getDeclaredFields();
//        for(int i=0; i< fields.length; i++){
//            System.out.println("Field " + (i+1) +  Modifier.toString(fields[i].getModifiers())
//                    +" "+ fields[i].getType().getName()+" "+ fields[i].getName());
//        }
//
//
        /**
         *  class<T>
         *   class <T> human<T>
         */

        Method method = Student.class.getMethod("getStringList", null);

        Type returnType = method.getGenericReturnType();

//        System.out.println(returnType);

        if(returnType instanceof ParameterizedType){

            ParameterizedType type = (ParameterizedType) returnType;

            Type[] typeArguments = type.getActualTypeArguments();
            for(Type typeArgument: typeArguments){
                Class typeArgs = (Class) typeArgument;
                System.out.println("TYpe::"+ typeArgs);
            }

        }


        method = Student.class.getMethod("setStringList", List.class, String.class, List.class);

        Type[] genericParams = method.getGenericParameterTypes();

        for(Type genericPara: genericParams) {
            if (genericPara instanceof ParameterizedType) {
                ParameterizedType type = (ParameterizedType) genericPara;


            Type[] typeArguments = type.getActualTypeArguments();
            for (Type typeArgument : typeArguments) {
                Class typeArgs = (Class) typeArgument;
                System.out.println("TYpe::" + typeArgs);
            }
        }
        }
    }
}
