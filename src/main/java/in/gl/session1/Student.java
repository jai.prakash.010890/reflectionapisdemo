package in.gl.session1;

import java.util.ArrayList;
import java.util.List;

public class Student {
    private String name;
    private String college;

    private int age;
    private Integer marks;

    private List<String> lst = new ArrayList<>();
    public Student(){

    }

    public List<String> getStringList(){
        return this.lst;
    }

    public void setStringList(List<String> lst, String name, List<Integer> lstInt){
        this.lst = lst;
    }

    public Student(String name, String college) {
        this.name = name;
        this.college = college;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }
}
