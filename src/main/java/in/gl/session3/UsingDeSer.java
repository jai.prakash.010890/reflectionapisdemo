package in.gl.session3;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Object of DeserializationExample class is serialized using writeObject() method and written to file.txt file.
 */
public class UsingDeSer {
    public static void main(String[] args) throws IOException {
        StudentNew studentNew = new StudentNew("jai");

        FileOutputStream f
                = new FileOutputStream("file.txt");
        ObjectOutputStream oos
                = new ObjectOutputStream(f);
        oos.writeObject(studentNew);
        oos.close();

        // Freeing up memory resources
        f.close();
    }
}
