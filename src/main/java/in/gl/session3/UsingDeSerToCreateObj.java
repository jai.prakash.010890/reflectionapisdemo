package in.gl.session3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class UsingDeSerToCreateObj {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        StudentNew studentNew;

        FileInputStream fileInputStream = new FileInputStream("file.txt");

        ObjectInputStream ois = new ObjectInputStream(fileInputStream);

        studentNew = (StudentNew) ois.readObject();

        System.out.println(studentNew.name);
    }
}
