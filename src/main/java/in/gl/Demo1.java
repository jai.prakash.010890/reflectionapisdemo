package in.gl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Demo1 {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {

//        User user = new User("jai", 100);
//
//        System.out.println(user.getName());


//        Class userClass = Class.forName("in.gl.User");
//
//        User user = (User) userClass.newInstance();
//
//        System.out.println(userClass.getName());
//
//        user.setName("jai");
//        System.out.println(user.getName());
//
//        System.out.println("--------------------");
//        Class userNI = Class.forName("in.gl.UserNewInstance");
//        UserNewInstance userNewInstance = (UserNewInstance) userNI.newInstance();
//
//        System.out.println(userNewInstance);

//        -------------
        Class userNI2 = Class.forName("in.gl.UserNewInstance2");

        Constructor[] arr = userNI2.getConstructors();
        for(int i=0; i<arr.length; i++)
            System.out.println(arr[i]);

        Constructor<?> ctr = userNI2.getConstructor(String.class, Integer.class);

        String name = "jai";
        Integer sal = 10;

        Object object = ctr.newInstance(new Object[]{name, sal});
        System.out.println(object);
        System.out.println(((UserNewInstance2)object).getName());

        Field [] fields = userNI2.getFields();
        for(int i=0; i< fields.length; i++)
            System.out.println(fields[i].getName());
//        UserNewInstance2 userNewInstance2 = (UserNewInstance2) userNI2.newInstance();

//        System.out.println(userNewInstance2);
    }
}
