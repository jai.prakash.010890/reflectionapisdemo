package in.gl.session2;

public class UserNew implements Cloneable{
    @Override
    protected Object clone()
            throws CloneNotSupportedException
    {
        // Super() keyword refers to parent class
        return super.clone();
    }
}
