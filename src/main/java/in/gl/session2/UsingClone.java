package in.gl.session2;

public class UsingClone {

    public static void main(String[] args) throws CloneNotSupportedException {
        UserNew userNew1 = new UserNew();

        UserNew userNew2 = (UserNew) userNew1.clone();
    }
}
