package in.gl.session5;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Demo {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Class softwareDeveloperClass = SoftwareDeveloper.class;

        Constructor constructor = softwareDeveloperClass.getConstructor( String.class, String.class, int.class);

        SoftwareDeveloper softwareDeveloper = (SoftwareDeveloper) constructor.newInstance("GL","Java", 10000);

        System.out.println(softwareDeveloper);

        // Now invoke methods
        Method method = softwareDeveloperClass.getMethod("getSalary");
        method.invoke(softwareDeveloper); // need to pass the object on which we want to invoe
        // invoke private method
        // getMethod vs getDeclared method

        Method method1 = softwareDeveloperClass.getDeclaredMethod("switchJobs");
        method1.setAccessible(true); // to solve error due to directly running invoke
        method1.invoke(softwareDeveloper);

        Field salaryField = softwareDeveloperClass.getDeclaredField("salary");
        salaryField.setAccessible(true);
        salaryField.set(softwareDeveloper, 999);
        System.out.println(softwareDeveloper);
    }
}
