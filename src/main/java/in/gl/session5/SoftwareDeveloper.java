package in.gl.session5;

public class SoftwareDeveloper {

    private String name;
    private String platform;
    private int salary;

    public SoftwareDeveloper(String name, String platform, int salary) {
        this.name = name;
        this.platform = platform;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public int getSalary() {
        System.out.println(salary);
        return salary;
    }

    private void switchJobs(){
        System.out.println("Keep practising");
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "SoftwareDeveloper{" +
                "name='" + name + '\'' +
                ", platform='" + platform + '\'' +
                ", salary=" + salary +
                '}';
    }
}
